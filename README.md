<h1>Due Date</h1>

Assignment Scheduling System Written In Python 3.3<br>
By: Joseph Gambino<br>
	<gambino@csh.rit.edu><br>
	<https://github.com/joegiants182><br>
	<https://github.com/joegiants182/duedate><br>

<h2>System Requirements:</h2>
	- Windows XP/Vista/7/8, any UNIX Based OS
	- Python 3.3, older versions may work, not tested nor supported

<h2>How To Use:</h2>
	1. Set your own configuration to your liking in config.txt (descriptions of fields in file)
	2. Add assignments into assignments.txt (formatting shown in file)
	3. Execute duedate.py using Python 3.3
	4. Add assignments while the program is running to assignments.txt and save it, the
	   program will automatically update to add your new assignments.
	   **Note: If you improperly format the assignment or add any unusual lines to the file
	   		   the program will crash. Simply fix the errors and re-run the program


<h3>Version History:</h3>

1.6 Added UNIX support

1.5: Added configuration option to set an amount of time past their due date that assignments will be
	 deleted after, and split config options and assignments into separate files

1.4: Added configuration option to allow for hiding assignments with due dates greater than the user 
	 defined amount of days

1.3: Added configuration option to allow for showing/hiding the hours:min:sec remaining if the assignment
	is more than one whole day from due.

1.2: Changed config variables to a dictionary for easier adding of new features

1.1: Added configuration options, in particular allowed for sorting by closest/farthest from due date
	and showing/hiding year in due date

1.0: Displays assignments, their due dates, and the time remaining. All basic functionality works