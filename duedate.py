'''
File: duedate.py
Language: Python 3.3
Author: Joseph Gambino <gambino@csh.rit.edu>
Version 1.5
'''

import datetime as t
import os
import time

# Put the location of your assignments.txt file here, make sure you escape your characters.
# global variables set by config
config = {  'assignments':'', \
			'sortType':'', \
			'showYear':['',0], \
			'showDaysOnly':['',1], \
			'cutOffDays':100, \
			'deleteAfter':0 }
clear = ['clear','cls'][os.name == 'nt']

def main():
	'''
	runs configuration and updates the screen
	'''
	global clear
	#global 	
	os.system(clear)
	configure()
	while True:
		printAssn()
		time.sleep(1)
		os.system(clear)
		# dueDateGUI.clear()
def printAssn():
	'''
	prints all assignments in configuration file
	'''
	# global config dictionary
	global config
	#global 	#curtime
	curr = t.datetime.today()
	#longest assignment name, 15 is the shortest because of the header
	longest = 15
	#lines to print, in tuple form i.e. (seconds remaining, line) for sorting purposes 
	lines = []
	# remove queue
	removeLst = []

	# finds the longest assignment name for scaling purposes
	for assn in open(config['assignments']):
		if assn[0] == '\"':
			assn = assn.split('\"')
			assn.remove('')
		else:
			assn = assn.split()
		if len(assn) == 2 and assn[0] != '#':
			if len(assn[0]) > longest:
				longest = len(assn[0])

	# sets header based on longest assignment name, and storesa size for ease of use
	header = '||Assignment Name' + ' '*(abs(longest-14)) + '|| Due Date' + ' '*(12-config['showYear'][1]) + '|| Time Remaining' + ' '*config['showDaysOnly'][1] + '||'
	size = len(header)-4

	# prints top border, header, and separator
	print('||' + '='*size + '||')
	print(header)
	print('||' + '='*size + '||')

	# parses in the file and forms the assignment line strings
	for line in open(config['assignments']):
		# is the line an assignment?
		if line[0] == '\"':
			line = line.split('\"')
			line.remove('')
			# splits the due date into year, month, day, hour, minutes, seconds and creates a datetime object
			due = line[1].split('.')
			dueDate = t.datetime(int(due[0]),int(due[1]),int(due[2]),int(due[3]),int(due[4]),int(due[5]))
			# creates the timedelta object to represent the remaining time
			timeLeft = dueDate - curr
			timeLeftSec = timeLeft.seconds + timeLeft.days*86400
			timeLeftString = str(timeLeft).split('.')[0]
			# is the time remaining greater than the maximum the user wants displayed?
			if timeLeft.days <= config['cutOffDays']:
				# is the assignment over the delete after time?
				if timeLeftSec < config['deleteAfter']:
					removeLst.append(line[0])
				else:
					# is the assignment past due?
					if timeLeft.days < 0:
						timeLeftString = '0:00:00'
						timeLeftSec = 0
					# does the user want the hours:mins:secs displayed after the days remaining if > 0?
					elif timeLeft.days > 0:
						if config['showDaysOnly'][0]:
							timeLeftString = timeLeftString.split(',')[0]
					# determines the spacing nescessary for the current assignment
					spacing = longest - len(line[0]) + 1
					date = str(dueDate)
					# does the user want the year displayed?
					if not config['showYear'][0]:
						date = str(dueDate)[5::]

					# add the line to the list of lines to print
					lines.append((timeLeftSec, (line[0] + ' '*spacing + '|| ' + date + ' || ' + timeLeftString))) 

	# sort the lines from closes to farthest
	lines.sort()
	# does the user want it reverse sorted?
	if config['sortType'] == 'last':
		lines.reverse()
	#print the lines and the bottom border
	for l in lines:
		print('||' + l[1] + ' '*(size-len(l[1])) + '||')
	print('||' + '='*size + '||')

	if len(removeLst) > 0:
		print(removeLst)
		removeAssn(removeLst)

def configure():
	'''
	reads in the configuration from the config file and sets the appropriate variables
	'''
	global config

	for line in open('config.txt'):
		line = line.split()
		if len(line) > 1 and line[0] == '###':
			if line[1] == 'assignments':
				config['assignments'] = line[2]
			elif line[1] == 'sortType':
				config['sortType'] = line[2]
			elif line[1] == 'showYear':
				config['showYear'][0] = (line[2] == 'True')
				if not config['showYear'][0]: config['showYear'][1] = 5
			elif line[1] == 'showDaysOnly':
				config['showDaysOnly'][0] = (line[2] == 'True')
				if not config['showDaysOnly'][0]: config['showDaysOnly'][1] = 4
			elif line[1] == 'cutOffDays':
				config['cutOffDays'] = int(line[2])
			elif line[1] == 'deleteAfter':
				deleteAfter = line[2].split('.')
				if len(deleteAfter) > 1:
					config['deleteAfter'] = ((int(deleteAfter[0])*86400) + (int(deleteAfter[1])*3600) + (int(deleteAfter[2])*60) + int(deleteAfter[3]))*-1
				else: config['deleteAfter'] = int(deleteAfter[0])*-1

def removeAssn(removeLst):
	'''
	will remove lines from config file for assignments greater than one day old
	remove (list): list of assignments to remove
	'''
	count,lines = 0,[]
	for line in open(config['assignments']):
		line = line.split("\"")
		if len(line) ==  3 and line[1] in removeLst:
			lines.append(count)
		count += 1

	linesList = open(config['assignments']).readlines()
	for rem in lines:
		linesList.remove(linesList[rem])

	open(config['assignments'], 'w').writelines(linesList)

if __name__ == '__main__': main()